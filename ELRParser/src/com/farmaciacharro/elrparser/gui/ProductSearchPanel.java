package com.farmaciacharro.elrparser.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.farmaciacharro.elrparser.message.MessageParser;
import com.farmaciacharro.elrparser.message.table.MessageTable;

public class ProductSearchPanel extends JPanel {
	
	private static final long serialVersionUID = 183877596819230763L;

	private JTextField inputField;
	private JTextField outputField;
	private JTextField productField;
	
	private JCheckBox aMessageBox;
	private JCheckBox bMessageBox;
	private JCheckBox iMessageBox;
	private JCheckBox pMessageBox;
	
	private MessageTable data;
	
	private MessageParser parser;
	
	public ProductSearchPanel() {
		this.parser = new MessageParser();
		init();
	}
	
	private void init() {
		JLabel inputLabel = new JLabel("Origen"); 
		inputField = new JTextField();
		inputField.setEditable(false);
		JButton inputButton = new JButton("Elegir archivos");
		inputButton.addActionListener(new SearchPathListener(inputField, JFileChooser.DIRECTORIES_ONLY));
		
		JLabel outputLabel = new JLabel("Destino");
		outputField = new JTextField();
		outputField.setEditable(false);
		JButton outputButton = new JButton("Elegir destino");
		outputButton.addActionListener(new SearchPathListener(outputField, JFileChooser.DIRECTORIES_ONLY));
		
		JLabel productLabel = new JLabel("Producto");
		productField = new JTextField();
		JButton searchButton = new JButton("Buscar");
		searchButton.addActionListener(new SearchProductListener());
		
		
		JPanel selectionPanel = new JPanel();
		selectionPanel.setLayout(new GridLayout(3, 3));
		
		selectionPanel.add(inputLabel);
		selectionPanel.add(inputField);
		selectionPanel.add(inputButton);
		selectionPanel.add(outputLabel);
		selectionPanel.add(outputField);
		selectionPanel.add(outputButton);
		selectionPanel.add(productLabel);
		selectionPanel.add(productField);
		selectionPanel.add(searchButton);
		
		aMessageBox = new JCheckBox("Dispensaciones");
		bMessageBox = new JCheckBox("Stock");
		iMessageBox = new JCheckBox("Entradas");
		pMessageBox = new JCheckBox("Pregunta");

		JPanel messagePanel = new JPanel();
		messagePanel.setLayout(new GridLayout(1, 4));
		messagePanel.add(aMessageBox);
		messagePanel.add(bMessageBox);
		messagePanel.add(iMessageBox);
		messagePanel.add(pMessageBox);
		
		JPanel headerPanel = new JPanel();
		headerPanel.setLayout(new GridLayout(2,1));
		headerPanel.add(selectionPanel);
		headerPanel.add(messagePanel);
		
		data = new MessageTable();
		JTable table = new JTable(data);
		JScrollPane dataPanel = new JScrollPane(table);
		dataPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		setLayout(new BorderLayout());
		add(headerPanel, BorderLayout.NORTH);
		add(dataPanel, BorderLayout.CENTER);		
	}

	private void searchProduct() {
		String productId = productField.getText().trim();
		String directoryPath = inputField.getText().trim();
		String messagesFilter = "";
		if (aMessageBox.isSelected()) {
			messagesFilter += "A";
		}
		if (bMessageBox.isSelected()) {
			messagesFilter += "B";
		}
		if (iMessageBox.isSelected()) {
			messagesFilter += "I";
		}
		if (pMessageBox.isSelected()) {
			messagesFilter += "P";
		}
		if ((productId != "") && (messagesFilter != "") && (directoryPath != "")) {
			data.removeAll();
			try {
				parser.parse(directoryPath, messagesFilter, productId, data);
			} catch (Exception e) {
				
			}
		}
	}
	
	class SearchPathListener implements ActionListener {
		
		private JTextField destination;
		private int filter;
		
		private SearchPathListener(JTextField destination, int filter) {
			this.destination = destination;
			this.filter = filter;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser(destination.getText());
			fileChooser.setFileSelectionMode(filter);
			int selection = fileChooser.showSaveDialog(null);
			if (selection == JFileChooser.APPROVE_OPTION)
			{
				destination.setText(fileChooser.getSelectedFile().getAbsolutePath());
			}
		}
	}
	
	class SearchProductListener implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			searchProduct();
		}
	}	
}
