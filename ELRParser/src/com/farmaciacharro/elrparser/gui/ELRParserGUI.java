package com.farmaciacharro.elrparser.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class ELRParserGUI extends JFrame {

	private static final long serialVersionUID = 4037761625513331661L;

	private static ELRParserGUI instance;
	
	private ELRParserGUI() {
		init();
	}
	
	public static ELRParserGUI getInstance() {
		if (null == instance) {
			instance = new ELRParserGUI();
		}
		return instance;
	}
	
	private void init() {
		setSize(600, 400);
		setTitle("ELR Parser");

		JLabel ownerLabel = new JLabel("Desarrollado por Farmacia Charro. Todos los derechos reservados.");

		JPanel footerPanel = new JPanel();
		footerPanel.setLayout(new GridLayout(1, 1));
		footerPanel.add(ownerLabel);
		
		JTabbedPane mainPanel = new JTabbedPane();
		mainPanel.addTab("Producto", new ProductSearchPanel());
		mainPanel.addTab("Albaran", new DeliveryNoteSearchPanel());
		
		JMenu menuFile = new JMenu("Archivo");
		JMenu menuHelp = new JMenu("Ayuda");
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(menuFile);
		menuBar.add(menuHelp);
		
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(menuBar, BorderLayout.NORTH);
		getContentPane().add(mainPanel, BorderLayout.CENTER);
		getContentPane().add(footerPanel, BorderLayout.SOUTH);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
