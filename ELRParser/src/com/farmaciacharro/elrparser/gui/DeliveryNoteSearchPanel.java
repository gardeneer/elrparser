package com.farmaciacharro.elrparser.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.farmaciacharro.elrparser.gui.ProductSearchPanel.SearchPathListener;
import com.farmaciacharro.elrparser.gui.ProductSearchPanel.SearchProductListener;
import com.farmaciacharro.elrparser.message.DeliveryNoteParser;
import com.farmaciacharro.elrparser.message.table.DeliveryNoteTable;

public class DeliveryNoteSearchPanel extends JPanel {
	
	private static final long serialVersionUID = 2801440339989663710L;
	
	private JTextField inputField;
	private JTextField outputField;
	private JTextField deliveryNoteField;
	
	private JTextField startDateField;
	private JTextField endDateField;
	
	private DeliveryNoteTable data;

	private DeliveryNoteParser parser;

	public DeliveryNoteSearchPanel() {
		this.parser = new DeliveryNoteParser();
		init();
	}
	
	private void init() {
		JLabel inputLabel = new JLabel("Origen"); 
		inputField = new JTextField();
		inputField.setEditable(false);
		JButton inputButton = new JButton("Elegir archivos");
		inputButton.addActionListener(new SearchPathListener(inputField, JFileChooser.DIRECTORIES_ONLY));
		
		JLabel outputLabel = new JLabel("Destino");
		outputField = new JTextField();
		outputField.setEditable(false);
		JButton outputButton = new JButton("Elegir destino");
		outputButton.addActionListener(new SearchPathListener(outputField, JFileChooser.DIRECTORIES_ONLY));
		
		JLabel deliveryNoteLabel = new JLabel("Albaran");
		deliveryNoteField = new JTextField("");
		JButton searchButton = new JButton("Buscar");
		searchButton.addActionListener(new SearchDeliveryNoteListener());
		
		
		JPanel selectionPanel = new JPanel();
		selectionPanel.setLayout(new GridLayout(3, 3));
		
		selectionPanel.add(inputLabel);
		selectionPanel.add(inputField);
		selectionPanel.add(inputButton);
		selectionPanel.add(outputLabel);
		selectionPanel.add(outputField);
		selectionPanel.add(outputButton);
		selectionPanel.add(deliveryNoteLabel);
		selectionPanel.add(deliveryNoteField);
		selectionPanel.add(searchButton);

		JLabel startDateLabel = new JLabel("Inicio (yyyymmdd)");
		startDateField = new JTextField("");
		
		JLabel endDateLabel = new JLabel("Fin (yyyymmdd)");
		endDateField = new JTextField("");
		
		JPanel datePanel = new JPanel();
		datePanel.setLayout(new GridLayout(1, 4));
		
		datePanel.add(startDateLabel);
		datePanel.add(startDateField);
		datePanel.add(endDateLabel);
		datePanel.add(endDateField);

		JPanel headerPanel = new JPanel();
		headerPanel.setLayout(new GridLayout(2,1));
		headerPanel.add(selectionPanel);
		headerPanel.add(datePanel);
		
		data = new DeliveryNoteTable();
		JTable table = new JTable(data);
		JScrollPane dataPanel = new JScrollPane(table);
		dataPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		setLayout(new BorderLayout());
		add(headerPanel, BorderLayout.NORTH);
		add(dataPanel, BorderLayout.CENTER);		
	}
	
	private void searchDeliveryNote() {
		String startDate = startDateField.getText().trim();
		String endDate = endDateField.getText().trim();
		String directoryPath = inputField.getText().trim();
		String deliveryNote = deliveryNoteField.getText().trim();

		if ((deliveryNote != "") && (directoryPath != "")) {
			data.removeAll();
			try {
				parser.parse(directoryPath, startDate, endDate, deliveryNote, data);
			} catch (Exception e) {
				
			}
		}
	}

	class SearchPathListener implements ActionListener {
		
		private JTextField destination;
		private int filter;
		
		private SearchPathListener(JTextField destination, int filter) {
			this.destination = destination;
			this.filter = filter;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser(destination.getText());
			fileChooser.setFileSelectionMode(filter);
			int selection = fileChooser.showSaveDialog(null);
			if (selection == JFileChooser.APPROVE_OPTION)
			{
				destination.setText(fileChooser.getSelectedFile().getAbsolutePath());
			}
		}
	}
	
	class SearchDeliveryNoteListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			searchDeliveryNote();
		}
	}		
}
