package com.farmaciacharro.elrparser.message.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.farmaciacharro.elrparser.message.type.Message;

public class MessageTable extends AbstractTableModel {
	
	private static final long serialVersionUID = -7410904156865267586L;
	
	List<Message> messages;
	private String[] headers = new String[] {"Fecha", "Acci�n", "Mensaje"};
	
	public MessageTable() {
		messages = new ArrayList<Message>();
	}
	
	@Override
	public String getColumnName(int column) {
	    return headers[column];
	}

	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public int getRowCount() {
		return messages.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Message message = messages.get(rowIndex);
        switch (columnIndex){
            case 0:
                return message.getTime();
            case 1:
                return message.getType();
            case 2:
                return message.getMessage();
        }
        return "";
	}
	
	public void add(Message message) {
        int size = getRowCount();
        messages.add(message);
        fireTableRowsInserted(size, size);
    }

    public void removeAll() {
    	int size = Math.max(0, getRowCount() - 1);
    	messages.clear();
    	fireTableRowsDeleted(0, size);
    }
}
