package com.farmaciacharro.elrparser.message.table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

public class DeliveryNoteTable extends AbstractTableModel {
	
	private static final long serialVersionUID = -7410904156865267586L;
	
	List<String> products;
	Map<String, Integer> count;
	
	private String[] headers = new String[] {"C�digo", "Cantidad"};
	
	public DeliveryNoteTable() {
		products = new ArrayList<String>();
		count = new HashMap<String, Integer>();
	}
	
	@Override
	public String getColumnName(int column) {
	    return headers[column];
	}

	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public int getRowCount() {
		return products.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex){
            case 0:
                return products.get(rowIndex);
            case 1:
                return count.get(products.get(rowIndex));
        }
        return "";
	}
	
	public void add(String productId, String units) {
		int size = products.size();
		Integer newValue = Integer.valueOf(units);
		if (products.contains(productId)) {
			newValue += count.get(productId);
			int i = 0;
			for (String aux: products) {
				if (productId.compareTo(aux) == 0) {
					size = i;
					break;
				}
			}
		} else {
			products.add(productId);
		}
		count.put(productId, newValue);
        fireTableRowsInserted(size, size);
    }

    public void removeAll() {
    	int size = Math.max(0, getRowCount() - 1);
    	products.clear();
    	count.clear();
    	fireTableRowsDeleted(0, size);
    }
}
