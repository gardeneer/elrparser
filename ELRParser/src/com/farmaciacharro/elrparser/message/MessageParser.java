package com.farmaciacharro.elrparser.message;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.farmaciacharro.elrparser.message.table.MessageTable;
import com.farmaciacharro.elrparser.message.type.AMessageRequest;
import com.farmaciacharro.elrparser.message.type.AMessageResponse;
import com.farmaciacharro.elrparser.message.type.BMessageRequest;
import com.farmaciacharro.elrparser.message.type.BMessageResponse;
import com.farmaciacharro.elrparser.message.type.IMessageRequest;
import com.farmaciacharro.elrparser.message.type.IMessageResponse;
import com.farmaciacharro.elrparser.message.type.Message;
import com.farmaciacharro.elrparser.message.type.PMessageRequest;
import com.farmaciacharro.elrparser.message.type.PMessageResponse;


public class MessageParser {

	private boolean AMessage = false;
	private boolean BMessage = false;
	private boolean IMessage = false;
	private boolean PMessage = false;
	private String productId;
	private MessageTable data;
	
	public MessageParser() {}
	
	public void parse(String pathDirectory, String messagesFilter, String productId, MessageTable data) 
			throws Exception {
		this.productId = productId;
		this.data = data;

		if (messagesFilter.contains("A")) {
			AMessage = true;
		}
		if (messagesFilter.contains("B")) {
			BMessage = true;
		}
		if (messagesFilter.contains("I")) {
			IMessage = true;
		}
		if (messagesFilter.contains("P")) {
			PMessage = true;
		}
		
		try {
			dirDirectory(new File(pathDirectory));
		} catch (FileNotFoundException fnfe) {
			throw new Exception("El directorio es erroneo");
		}
		
	}
	
	private void dirDirectory(File directory) throws FileNotFoundException {
		File[] files = directory.listFiles();
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					dirDirectory(files[i]);
				} else {
					parserFile(files[i]);
				}
			}
		}
	}
	
	private void parserFile(File file) throws FileNotFoundException {
		System.out.println(file);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		int i = 1;
		try {
			Pattern pattern = Pattern.compile("(\\d{4})(\\d{2})(\\d{2}):(\\d{2})(\\d{2})(\\d{2}):(\\d{3})\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s+(\\w+):\\s+(\\w+)\\s+\\-\\>\\s+(\\w+):\\s+(\\w)(.+)");
			while ((line = br.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				if (matcher.find()) {
					String from = matcher.group(14);
					String to = matcher.group(15);
					String type = matcher.group(16);
					if ((AMessage) && (type.compareTo("A") == 0) && (from.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						Message message = new AMessageRequest(year, month, day, hour, minute, second, milliseconds, messageLine);
						if (message.isProductId(productId)) {
							data.add(message);
						}
					} else if ((AMessage) && (type.compareTo("a") == 0) && (to.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						Message message = new AMessageResponse(year, month, day, hour, minute, second, milliseconds, messageLine);
						if (message.isProductId(productId)) {
							data.add(message);
						}
					} else if ((BMessage) && (type.compareTo("B") == 0) && (from.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						Message message = new BMessageRequest(year, month, day, hour, minute, second, milliseconds, messageLine);
						if (message.isProductId(productId)) {
							data.add(message);
						}
					} else if ((BMessage) && (type.compareTo("b") == 0) && (to.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						Message message = new BMessageResponse(year, month, day, hour, minute, second, milliseconds, messageLine);
						if (message.isProductId(productId)) {
							data.add(message);
						}
					} else if ((IMessage) && (type.compareTo("i") == 0) && (from.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						Message message = new IMessageRequest(year, month, day, hour, minute, second, milliseconds, messageLine);
						if (message.isProductId(productId)) {
							data.add(message);
						}
					} else if ((IMessage) && (type.compareTo("I") == 0) && (to.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						Message message = new IMessageResponse(year, month, day, hour, minute, second, milliseconds, messageLine);
						if (message.isProductId(productId)) {
							data.add(message);
						}
					} else if ((PMessage) && (type.compareTo("p") == 0) && (to.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						Message message = new PMessageRequest(year, month, day, hour, minute, second, milliseconds, messageLine);
						if (message.isProductId(productId)) {
							data.add(message);
						}
					} else if ((PMessage) && (type.compareTo("P") == 0) && (from.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						Message message = new PMessageResponse(year, month, day, hour, minute, second, milliseconds, messageLine);
						if (message.isProductId(productId)) {
							data.add(message);
						}
					}
				} 
				i++;
			}
		} catch (Exception e) {
			System.out.println("Error en l�nea " + i);
			System.out.println(e.getMessage());
		} finally {
			if (null != fr) {
				try {
					fr.close();
				} catch (IOException e2) {
					System.out.println(e2.getMessage());
				}
			}
		}
	}
}
