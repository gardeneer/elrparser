package com.farmaciacharro.elrparser.message.type;

/*
 * Output
 * Output request from WMS
 */
public class AMessageRequest extends Message {

	private final static String TYPE = "A";
	private final static String SYMBOL = "<=";
	
	private String orderNumber;
	private String requestorId;
	private String destinationId;
	private String priority;
	private String [][] lines;
	
	public AMessageRequest(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		String [] tokens = message.split("\\|");
		orderNumber = tokens[1];
		requestorId = tokens[2];
		destinationId = tokens[3];
		priority = tokens[4];
		int lineCounter = Integer.valueOf(tokens[5]);
		lines = new String[lineCounter][3];
		for (int i = 0; i < lineCounter; i++) {
			int base = 6 + (i * 3); 
			lines[i][0] = tokens[base++]; // productId
			lines[i][1] = tokens[base++]; // productCount to pick
			lines[i][2] = tokens[base++]; //storageFlat
		}
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @return the requestorId
	 */
	public String getRequestorId() {
		return requestorId;
	}

	/**
	 * @return the destinationId
	 */
	public String getDestinationId() {
		return destinationId;
	}

	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * @return the lines
	 */
	public String[][] getLines() {
		return lines;
	}
	
	public String getMessage() {
		String aux = " Order by " + requestorId + ". Output: " + destinationId + ".";
		for (int i = 0; i < lines.length; i++) {
			aux += " ProductID = " + lines[i][0] + "(" + lines[i][1] + ").";
		}
		return aux;
	}
	
	public boolean isProductId(String productId) {
		for (int i = 0; i < lines.length; i++) {
			if (lines[i][0].endsWith(productId)) {
				return true;
			}
		}
		return false; 
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}
	
}
