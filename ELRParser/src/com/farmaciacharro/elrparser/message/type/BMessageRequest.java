package com.farmaciacharro.elrparser.message.type;

/**
 * Product count and storage information messages
 * Message from WMS: request for Product count and storage information
 * @author Ignacio Colino Cortizo
 * @version 1.0
 */
public class BMessageRequest extends Message {

	private final static String TYPE = "B";
	private final static String SYMBOL = "=?";
	
	private String requestorId;
	private String productId;
	
	public BMessageRequest(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		String [] tokens = message.split("\\|");
		requestorId = tokens[1];
		productId = tokens[2];
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return the requestorId
	 */
	public String getRequestorId() {
		return requestorId;
	}

	public String getMessage() {
		return " ProductId = " + productId + ".";
	}
	
	public boolean isProductId(String productId) {
		return (this.productId.endsWith(productId));
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}
	
}
