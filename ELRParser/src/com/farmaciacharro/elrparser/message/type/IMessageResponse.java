package com.farmaciacharro.elrparser.message.type;

/**
 * Input
 * Input into Apostore-System, controlled bt WMS
 * @author Ignacio Colino Cortizo
 * @version 1.0
 */
public class IMessageResponse extends Message {

	private final static String TYPE = "IR";
	private final static String SYMBOL = "=>";
	
	private String orderNumber;
	private String requestorId;
	private String productId;
	private String productCount;
	private String expiryDate;
	private String state;
	private String text;
	
	public IMessageResponse(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		orderNumber = message.substring(0, 8);
		requestorId = message.substring(8, 11);
		productId = message.substring(11, 18);
		productCount = message.substring(18, 23);
		expiryDate = message.substring(23, 29);
		state = message.substring(29, 31);
		text = message.substring(31);
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @return the requestorId
	 */
	public String getRequestorId() {
		return requestorId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return the productCount
	 */
	public String getProductCount() {
		return productCount;
	}

	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	

	public String getMessage() {
		return " Order by " + requestorId + ". ProductID = " + productId + ". Cantidad = " + productCount + "." +
				"Caducidad: " + expiryDate + ". Estado = " + iMessageState(state) + ". Texto: " + text;
	}
	
	public boolean isProductId(String productId) {
		return this.productId.endsWith(productId);
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}
	
	private static String iMessageState(String state) {
		switch(Integer.valueOf(state)) {
			case 0: 
				return "Permitida";
			case 1: 
				return "Denegada";
			case 2: 
				return "Denegada. Requiere caducidad";
			case 4: 
				return "Denegada. Sin producto";
			case 5: 
				return "Requiere nevera";
		}
		return "Sin definir";
	}		
}
