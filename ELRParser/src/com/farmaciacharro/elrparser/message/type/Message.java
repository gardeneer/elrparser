package com.farmaciacharro.elrparser.message.type;

public abstract class Message {

	protected String year;
	protected String month;
	protected String day;
	protected String hour;
	protected String minute;
	protected String second;
	protected String millisecond;
	protected String type;
	
	protected Message(String year, String month, String day, String hour, String minute, String second, String millisecond, String type) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.hour = hour;
		this.minute = minute;
		this.second = second;
		this.millisecond = millisecond;
		this.type = type;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @return the hour
	 */
	public String getHour() {
		return hour;
	}

	/**
	 * @return the minute
	 */
	public String getMinute() {
		return minute;
	}

	/**
	 * @return the second
	 */
	public String getSecond() {
		return second;
	}
	
	/**
	 * @return the millisecond
	 */
	public String getMillisecond() {
		return millisecond;
	}
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	public abstract String getMessage();
	
	public abstract boolean isProductId(String productId);
	
	public String getTime() {
		return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second + "." + millisecond;
	}
	
	public String toString() {
		return year + "/" + month + "/" + day + " " + hour + ":" + minute + ":" + second + "." + millisecond;
	}
}
