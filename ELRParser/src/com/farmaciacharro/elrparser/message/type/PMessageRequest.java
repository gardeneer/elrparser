package com.farmaciacharro.elrparser.message.type;

/**
 * Product information
 * Product information request from Apostore
 * @author Ignacio Colino Cortizo
 * @version 1.0
 */
public class PMessageRequest extends Message {

	private final static String TYPE = "P";
	private final static String SYMBOL = "=!";
	
	private String requestorId;
	private String productId;
	private String scancode;
	
	public PMessageRequest(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		String [] tokens = message.split("\\|");
		requestorId = tokens[1];
		productId = tokens[2];
		//scancode = tokens[3];
	}

	/**
	 * @return the requestorId
	 */
	public String getRequestorId() {
		return requestorId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return the scancode
	 */
	public String getScancode() {
		return scancode;
	}

	public String getMessage() {
		return " Order by " + requestorId + ". ProductId: " + productId + ". Scancode: " + scancode;
	}
	
	public boolean isProductId(String productId) {
		return (this.productId.endsWith(productId)); 
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}	
}
