package com.farmaciacharro.elrparser.message.type;

/**
 * Product count and storage information messages
 * message from Apostare - response or asynchronous message
 */
public class BMessageResponse extends Message {

	private final static String TYPE = "BR";
	private final static String SYMBOL = "=?R";
	
	private String requestorId;
	private String productId;
	private String sumCount;
	private String [][] lines;
	
	public BMessageResponse(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		String [] tokens = message.split("\\|");
		requestorId = tokens[1];
		productId = tokens[2];
		sumCount = tokens[3];
		int lineCounter = Integer.valueOf(tokens[4]);
		lines = new String[lineCounter][4];
		for (int i = 0; i < lineCounter; i++) {
			int base = 5 + (i * 4); 
			lines[i][0] = tokens[base++]; // storageLocation
			lines[i][1] = tokens[base++]; // max productCount
			lines[i][2] = tokens[base++]; // current packages in location
			lines[i][3] = tokens[base++]; // expiryDate
		}
	}

	
	/**
	 * @return the requestorId
	 */
	public String getRequestorId() {
		return requestorId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return the sumCount
	 */
	public String getSumCount() {
		return sumCount;
	}

	/**
	 * @return the lines
	 */
	public String[][] getLines() {
		return lines;
	}

	public String getMessage() {
		return " Order by " + requestorId + ". ProductId: " + productId + ". Cantidad: " + sumCount + ".";
	}
	
	public boolean isProductId(String productId) {
		return (this.productId.endsWith(productId)); 
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}
}
