package com.farmaciacharro.elrparser.message.type;

/**
 * Input
 * Input into Apostore-System, controlled bt WMS
 * @author Ignacio Colino Cortizo
 * @version 1.0
 */
public class IMessageRequest extends Message {

	private final static String TYPE = "I";
	private final static String SYMBOL = "=>";
	
	private String orderNumber;
	private String requestorId;
	private String deliveryNote;
	private String productId;
	private String scancode;
	private String productCount;
	private String expiryDate;
	private String state;
	
	public IMessageRequest(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		orderNumber = message.substring(0, 8);
		requestorId = message.substring(8, 11);
		deliveryNote = message.substring(11, 23);
		productId = message.substring(23, 30);
		scancode = message.substring(30, 43);
		productCount = message.substring(43, 48);
		expiryDate = message.substring(48, 54);
		state = message.substring(54, 56);
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @return the requestorId
	 */
	public String getRequestorId() {
		return requestorId;
	}

	/**
	 * @return the deliveryNote
	 */
	public String getDeliveryNote() {
		return deliveryNote;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return the scancode
	 */
	public String getScancode() {
		return scancode;
	}

	/**
	 * @return the productCount
	 */
	public String getProductCount() {
		return productCount;
	}

	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	public String getMessage() {
		return " Order by " + requestorId + ". Albaran: " + deliveryNote + "." +
				"ProductID = " + productId + ". Cantidad = " + productCount + "." +
				"Estado = " + iMessageState(state);
	}
	
	public boolean isProductId(String productId) {
		return this.productId.endsWith(productId);
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}
	
	private static String iMessageState(String state) {
		switch(Integer.valueOf(state)) {
			case 0: 
				return "Entrada de nuevos paquetes";
			case 1: 
				return "Entrada de retorno";
			case 2: 
				return "Inicio de entrada con albar�n";
			case 3: 
				return "Fin de entrada con albar�n";
			case 4: 
				return "Entrada de retorno marcando como 'robotable'";
			case 5: 
				return "Entrada de nuevos marcando como 'robotables'";
			case 6:
				return "Entrada correcta";
			case 7:
				return "Entrada cancelada";
		}
		return "Sin definir";
	}		
}
