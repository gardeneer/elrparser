package com.farmaciacharro.elrparser.message.type;

/**
 * Product count and storage information messages
 * message from Apostare - response or asynchronous message
 */
public class PMessageResponse extends Message {

	private final static String TYPE = "PR";
	private final static String SYMBOL = "=!R";
	
	private String requestorId;
	private String productId;
	private String productName;
	private String presentation;
	private String pieces;
	private String scancode;
	private String expiryDate;
	
	public PMessageResponse(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		String [] tokens = message.split("\\|");
		requestorId = tokens[1];
		productId = tokens[2];
		productName = tokens[3];
		presentation = tokens[4];
		pieces = tokens[5];
		scancode = tokens[6];
		expiryDate = tokens[7];
	}
	
	/**
	 * @return the requestorId
	 */
	public String getRequestorId() {
		return requestorId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @return the presentation
	 */
	public String getPresentation() {
		return presentation;
	}

	/**
	 * @return the pieces
	 */
	public String getPieces() {
		return pieces;
	}

	/**
	 * @return the scancode
	 */
	public String getScancode() {
		return scancode;
	}

	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	public String getMessage() {
		return " Order by " + requestorId + ". ProductId: " + productId + ". Nombre: " + productName + 
				". Presentacion: " + presentation + ". Volumen: " + pieces + ". C�digo de barras: " + 
				scancode + ". Caducidad: " + expiryDate;
	}
	
	public boolean isProductId(String productId) {
		return (this.productId.endsWith(productId)); 
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}
}
