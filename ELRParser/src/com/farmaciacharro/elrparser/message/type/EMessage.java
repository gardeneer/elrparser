package com.farmaciacharro.elrparser.message.type;

/**
 * Product count and storage information messages
 * Message from WMS: request for Product count and storage information
 * @author Ignacio Colino Cortizo
 * @version 1.0
 */
public class EMessage extends Message {

	private final static String TYPE = "E";
	private final static String SYMBOL = "##";
	
	private String orderNumber;
	private String productId;
	private String scancode;
	private String productCount;
	private String expiryDate;
	private String deliveryNote;
	private String state;
	
	public EMessage(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		String [] tokens = message.split("\\|");
		orderNumber = tokens[1];
		productId = tokens[2];
		scancode = tokens[3];
		productCount = tokens[4];
		expiryDate = tokens[5];
		deliveryNote = tokens[14];
		state = tokens[16];
	}


	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @return the scancode
	 */
	public String getScancode() {
		return scancode;
	}

	/**
	 * @return the productCount
	 */
	public String getProductCount() {
		return productCount;
	}

	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @return the deliveryNote
	 */
	public String getDeliveryNote() {
		return deliveryNote;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	public String getMessage() {
		return " ProductId = " + productId + ". Cantidad: " + productCount + 
				". Albaran: " + deliveryNote + ". Estado: " + eMessageState(state);
	}
	
	public boolean isProductId(String productId) {
		return (this.productId.endsWith(productId));
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}
	
	private static String eMessageState(String state) {
		switch(Integer.valueOf(state)) {
			case 0: 
				return "Permitida";
			case 1: 
				return "Denegada";
			case 2: 
				return "Denegada. Requiere caducidad";
			case 4: 
				return "Denegada. Sin producto";
			case 5: 
				return "Requiere nevera";
		}
		return "Sin definir";
	}		
}
