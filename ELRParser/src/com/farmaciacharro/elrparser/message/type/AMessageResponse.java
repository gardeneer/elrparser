package com.farmaciacharro.elrparser.message.type;

/*
 * Output
 * Output response from Apostore
 */
public class AMessageResponse extends Message {

	private final static String TYPE = "AR";
	private final static String SYMBOL = "<=R";
	
	private String orderNumber;
	private String requestorId;
	private String destinationId;
	private String state;
	private String [][] lines;
	
	public AMessageResponse(String year, String month, String day, String hour, String minute, String second, 
			String millisecond, String message) {
		super(year, month, day, hour, minute, second, millisecond, TYPE);
		parseMessage(message);
	}
	
	private void parseMessage(String message) {
		String [] tokens = message.split("\\|");
		orderNumber = tokens[1];
		requestorId = tokens[2];
		destinationId = tokens[3];
		state = tokens[4];
		int lineCounter = Integer.valueOf(tokens[5]);
		lines = new String[lineCounter][3];
		for (int i = 0; i < lineCounter; i++) {
			int base = 6 + (i * 2); 
			lines[i][0] = tokens[base++]; // productId
			lines[i][1] = tokens[base++]; // productCount picked
		}
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @return the requestorId
	 */
	public String getRequestorId() {
		return requestorId;
	}

	/**
	 * @return the destinationId
	 */
	public String getDestinationId() {
		return destinationId;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @return the lines
	 */
	public String[][] getLines() {
		return lines;
	}
	
	public String getMessage() {
		String aux = " Order by " + requestorId + ". Output: " + destinationId + ". Estado: " + aMessageState(state) + ".";
		for (int i = 0; i < lines.length; i++) {
			aux += " ProductID = " + lines[i][0] + "(" + lines[i][1] + ").";
		}
		return aux;
	}
	
	public boolean isProductId(String productId) {
		for (int i = 0; i < lines.length; i++) {
			if (lines[i][0].endsWith(productId)) {
				return true;
			}
		}
		return false; 
	}
	
	public String toString() {
		return super.toString() + " " + SYMBOL + getMessage();
	}
	
	private static String aMessageState(String state) {
		switch(Integer.valueOf(state)) {
			case 0: 
				return "Ocupado";
			case 1: 
				return "En cola";
			case 2: 
				return "Cancelado por cola llena";
			case 3: 
				return "Cancelado";
			case 4: 
				return "Preparado";
			case 5: 
				return "Cambio cantidad";
		}
		return "Sin definir";
	}	
}
