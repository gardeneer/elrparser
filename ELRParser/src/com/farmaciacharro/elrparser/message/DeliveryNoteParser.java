package com.farmaciacharro.elrparser.message;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.farmaciacharro.elrparser.message.table.DeliveryNoteTable;
import com.farmaciacharro.elrparser.message.type.EMessage;


public class DeliveryNoteParser {

	private String deliveryNote;
	private String startDate;
	private String endDate;
	private DeliveryNoteTable data;
	
	public DeliveryNoteParser() {}
	
	public void parse(String pathDirectory, String startDate, String endDate, String deliveryNote, DeliveryNoteTable data) 
			throws Exception {
		this.deliveryNote = deliveryNote;
		this.startDate = (startDate.isEmpty()) ? "19700101" : startDate;
		this.endDate = (endDate.isEmpty()) ? "99991231" : endDate;
		this.data = data;

		try {
			dirDirectory(new File(pathDirectory));
		} catch (FileNotFoundException fnfe) {
			throw new Exception("El directorio es erroneo");
		}
		
	}
	
	private void dirDirectory(File directory) throws FileNotFoundException {
		File[] files = directory.listFiles();
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					dirDirectory(files[i]);
				} else {
					parserFile(files[i]);
				}
			}
		}
	}
	
	private void parserFile(File file) throws FileNotFoundException {
		System.out.println(file);
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String line;
		int i = 1;
		try {
			Pattern pattern = Pattern.compile("(\\d{4})(\\d{2})(\\d{2}):(\\d{2})(\\d{2})(\\d{2}):(\\d{3})\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s+(\\w+)\\s+(\\w+):\\s+(\\w+)\\s+\\-\\>\\s+(\\w+):\\s+(\\w)(.+)");
			while ((line = br.readLine()) != null) {
				Matcher matcher = pattern.matcher(line);
				if (matcher.find()) {
					String from = matcher.group(14);
					String to = matcher.group(15);
					String type = matcher.group(16);
					if ((type.compareTo("E") == 0) && (from.compareToIgnoreCase("ELR") == 0)) {
						String year = matcher.group(1);
						String month = matcher.group(2);
						String day = matcher.group(3);
						String hour = matcher.group(4);
						String minute = matcher.group(5);
						String second = matcher.group(6);
						String milliseconds = matcher.group(7);
						String messageLine = matcher.group(17);
						EMessage message = new EMessage(year, month, day, hour, minute, second, milliseconds, messageLine);
						if ((message.getDeliveryNote().trim().compareToIgnoreCase(deliveryNote) == 0) 
								&& (message.getState().compareTo("00") == 0)){
							data.add(message.getProductId(), message.getProductCount());
						}
					} 				
				}
				i++;
			}
		} catch (Exception e) {
			System.out.println("Error en l�nea " + i);
			System.out.println(e.getMessage());
		} finally {
			if (null != fr) {
				try {
					fr.close();
				} catch (IOException e2) {
					System.out.println(e2.getMessage());
				}
			}
		}
	}
}
