package net.icolino.util.log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Log {

	static private FileHandler fileTxt;

	static private SimpleFormatter formatterTxt;
	
	public static final String LOGNAME = "LOGNAME";
	
	static public void setup(String filename) throws IOException {
	    Logger logger = Logger.getLogger(LOGNAME); 

	    fileTxt = new FileHandler(filename + ".log");

	    // Create txt Formatter
	    formatterTxt = new SimpleFormatter();
	    fileTxt.setFormatter(formatterTxt);
	    fileTxt.setLevel(Level.ALL);
	    logger.addHandler(fileTxt);
	}
}
